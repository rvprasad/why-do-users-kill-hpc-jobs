# README

For our effort described in the manuscript [Why do Users Kill HPC Jobs?](https://bitbucket.org/rvprasad/why-do-users-kill-hpc-jobs/downloads/hipcupp-1-TR.pdf), we gathered the reasons why users kill their jobs on [Beocat HPC cluster](https://support.beocat.ksu.edu/BeocatDocs/index.php/Main_Page) at [Kansas State University](http://www.k-state.edu/).  This repository contains the analysis scripts and data used in our effort.


## Replication

1.  Unzip the *bz2* files found in *input* and its descendant folders in the folders they found in.
2.  Make sure your system has *sed, awk,* and *uniq* commands.
3.  Run `scripts/masterScript.sh` from within bash shell.


## Data

We removed the names of all users who ran jobs and provided reasons for killing jobs from the data set via anonymization (when they occurred in identifiable positions).

### Input

-   **input/sge_accounting_file-08152016-12312017.csv** contains the fragment of [SGE Accounting File](https://linux.die.net/man/5/sge_accounting) from Beocat cluster for the duration considered in our effort.
-   **input/aux-data/categories.txt** maps categories of reasons to short ids.
-   **input/aux-data/raw-categorization.txt** maps the reasons provided by users to categories.  Fields in order:
    -   short (reason) category id,
    -   username, and
    -   user provided reason.
-   **input/raw-feedback/output*csv** ontains the reasons for job deletion as captured via `qdel` command.  Fields in order:
    -   username,
    -   jobid,
    -   apps used in the job,
    -   reason for killing the job, and
    -   dump of info about job.  This dump is a "!!!" separated record.
-   **input/raw-feedback/Job-Feedback-08152016-12312017.csv** contains the reasons for job deletion as captured via a web form.  Fields in order:
    -   timestamp,
    -   username,
    -   jobid,
    -   reason for killing the job,
    -   apps used in the job, and
    -   comments.


### Output

-   **output/interim/raw-web-feedback.csv** consolidates username, jobid, apps used in the job, and reason for killing the job from *input/raw-feedback/Job-Feedback-08152016-12312017.csv*.
-   **output/interim/raw-qdel-feedback.csv** consolidates username, jobid, apps used in the job, and reason for killing the job from *input/raw-feedback/output*.csv*.
-   **output/interim/raw-consolidated-feedback.csv** consolidates data from *interim/raw-qdel-feedback.csv* and *interim/raw-web-feedback.csv*.
-   **output/categorization.txt** contains (re)anonymized categorization of reasons from *input/aux-data/raw-categorization.txt*.  Reanonymization exists for historical purpose and to keep the changes to the earlier workflow minimal.
-   **output/feedback.csv** contains (re)anonymized feedback/reasons from *interim/raw-consolidated-feedback.csv*.
-   **output/feedback-trend.csv** contains the time trend of feedback/reason frequency.
-   **output/feedback-categories-trend.csv** contains the time trend of feedback/reason categories (across groups of 1000 users).
-   **output/feedback-categories-stats.csv** contains the total amount of cpu time, memory, io, and max vmem consumed by jobs killed for (a category of) reason.
-   **output/sge-time-info.csv** contains total amount of wait and execution time (both wall clock and cpu) consumed by jobs exiting with specific exit code according to SGE accounting file.
-   **output/sge-based-qdel-time-info.csv** contains wait and execution time (both wall clock and cpu) consumed by killed jobs and we have some info about the reason for killing them.
-   **output/sge-based-qdel-time-info.csv** contains total amount of wait and execution times (both wall clock and cpu) consumed by jobs killed for specific reason.

## Attribution

Copyright (c) 2018, Kansas State University

Licensed under [BSD 3-clause "New" or "Revised" License](https://choosealicense.com/licenses/bsd-3-clause/)

**Authors:** Venkatesh-Prasad Ranganath
