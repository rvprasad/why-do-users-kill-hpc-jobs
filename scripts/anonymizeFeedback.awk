# Copyright (c) 2017, Kansas State University
#
# BSD 3-clause License
#
# Author: Venkatesh-Prasad Ranganath

BEGIN {
    FS = ","
    numOfUsers = 0
    OFS = ","
}

/.+,.*,.*,.*/ {
    if (!($1 in users)) {
        numOfUsers = numOfUsers + 1
        users[$1] = numOfUsers
    }
    # flip order of job id and user id
    print $2, users[$1], $3, $4, $5, $6, $7, $8
}

END {
    system("rm " user2idFile)
    for (i in users) {
        print i, users[i] >> user2idFile
    }
}
