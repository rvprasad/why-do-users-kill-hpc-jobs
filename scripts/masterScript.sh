#!/usr/bin/env bash

# Copyright (c) 2017, Kansas State University
#
# BSD 3-clause License
#
# Author: Venkatesh-Prasad Ranganath

echo "Requires awk, sed, and uniq tools"

TIME_PERIOD="08152016-12312017"
RAW_WEB_FEEDBACK=input/raw-feedback/Job-Feedback-$TIME_PERIOD.csv
RAW_QDEL_INTERIM_FEEDBACK=output/interim/raw-qdel-feedback.csv
RAW_WEB_INTERIM_FEEDBACK=output/interim/raw-web-feedback.csv
RAW_CONSOLIDATED_INTERIM_FEEDBACK=output/interim/raw-consolidated-feedback.csv
ANONYMIZED_FEEDBACK=output/anonymized-feedback.csv
FEEDBACK2CATEGORY_MAPPING_SED_SCRIPT=scripts/feedback2category.sed
FEEDBACK=output/feedback.csv
FEEDBACK_CAT_STATS=output/feedback-categories-stats.csv
RAW_CATEGORIZATION=input/aux-data/raw-categorization.txt
CATEGORIES_FILE=input/aux-data/categories.txt
CATEGORIZATION=output/categorization.txt
USER_ID_FILE=output/interim/user-2-id.txt
FEEDBACK_CAT_TREND=output/feedback-categories-trend.csv
SGE_DATA_FILE=input/sge_accounting_file-$TIME_PERIOD.csv
SGE_TIME_INFO=output/sge-time-info.csv
SGE_BASED_QDEL_TIME_INFO=output/sge-based-qdel-time-info.csv
SGE_BASED_CAT_STATS=output/sge-categories-stats.csv
FEEDBACK_TREND=output/feedback-trend.csv
MAXVMEM_FREQ_DIST=output/sge-based-maxvmem-freq-dist.csv

mkdir -p output/interim

echo "$(date) Extracting feedback from qdel data"
awk -f scripts/extractFeedbackFromQDelData.awk input/raw-feedback/output*csv \
    > $RAW_QDEL_INTERIM_FEEDBACK

echo "$(date) Extracting feedback from web data"
awk -v rawFeedbackFile=$RAW_WEB_FEEDBACK \
    -f scripts/extractFeedbackFromWebFormData.awk $SGE_DATA_FILE \
    > $RAW_WEB_INTERIM_FEEDBACK

echo "$(date) Consolidating feedback data"
awk -v webFeedbackFile=$RAW_WEB_INTERIM_FEEDBACK \
    -f scripts/consolidateQdelAndWebFeedbacks.awk \
    $RAW_QDEL_INTERIM_FEEDBACK > $RAW_CONSOLIDATED_INTERIM_FEEDBACK

echo "$(date) Anonymize data"
awk -v user2idFile=$USER_ID_FILE \
    -f scripts/anonymizeFeedback.awk $RAW_CONSOLIDATED_INTERIM_FEEDBACK \
    > $ANONYMIZED_FEEDBACK
awk -v user2idFile=$USER_ID_FILE \
    -f scripts/anonymizeCategorization.awk $RAW_CATEGORIZATION \
    > $CATEGORIZATION

echo "$(date) Mapping feedback to categories"
awk -f scripts/generatefeedback2category.awk $CATEGORIZATION \
    | uniq > $FEEDBACK2CATEGORY_MAPPING_SED_SCRIPT
sed -E -f $FEEDBACK2CATEGORY_MAPPING_SED_SCRIPT $ANONYMIZED_FEEDBACK \
    > $FEEDBACK

echo "$(date) Generating descriptive stats of categories from feedback"
awk -f scripts/getDescriptiveStatsOfCategoriesFromFeedback.awk $FEEDBACK \
    > $FEEDBACK_CAT_STATS

echo "$(date) Generating feedback trend"
awk -v categoriesFile=$CATEGORIES_FILE \
    -f scripts/getFeedbackCategoriesTrend.awk $FEEDBACK > $FEEDBACK_CAT_TREND
awk -v qdelFeedbackFile=$RAW_WEB_FEEDBACK \
    -f scripts/getFeedbackTrend.awk input/raw-feedback/output-*csv \
    > $FEEDBACK_TREND

echo "$(date) Generating run time info from SGE data"
awk -f scripts/getCPUInfoFromSGEData.awk $SGE_DATA_FILE > $SGE_TIME_INFO

echo "$(date) Generating run time info for qdeled jobs from SGE data"
awk -v feedbackFile=$FEEDBACK \
    -f scripts/getCPUInfoForQDeledJobsFromSGEData.awk \
    $SGE_DATA_FILE > $SGE_BASED_QDEL_TIME_INFO

echo "$(date) Generating descriptive stats of categories from SGE data"
awk -v feedbackFile=$FEEDBACK \
    -f scripts/getDescriptiveStatsOfCategoriesFromSGEData.awk \
    $SGE_DATA_FILE > $SGE_BASED_CAT_STATS
