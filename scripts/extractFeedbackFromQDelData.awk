# Copyright (c) 2017, Kansas State University
#
# BSD 3-clause License
#
# Author: Venkatesh-Prasad Ranganath

BEGIN {
    FS = ","
    OFS = ","
}

{
    usageLine = getUsageLine($5)
    gsub(/^[^:]*:/, "", usageLine)
    gsub(/[[:space:]]/, "", usageLine)
    split(usageLine, usage, "!@!@")
    for (i in usage) {
        gsub(/.*=/, "", usage[i])
    }

    cpu = convertTimeToSecs(usage[1])
    gsub(/GBs$/, "", usage[2])
    mem = convertToMBs(usage[2])
    gsub(/B$/, "", usage[3])
    io = convertToMBs(usage[3])
    maxvmem = convertToMBs(usage[5])

    gsub(/,/, ";", $3)
    gsub(/,/, ";", $4)

    jobIdsString = $2
    gsub(/[[:alpha:]]/, "", jobIdsString)
    gsub(/[[:space:]]/, "", jobIdsString)
    gsub(/-/, "", jobIdsString)
    gsub(/,/, ";", jobIdsString)

    # username, jobid, app, reason, cpu (s), mem (MBs), io (MB), maxvmem (MB)
    printf "%s,%s,%s,%s,%.3f,%.3f,%.3f,%.3f\n", $1, jobIdsString, $3, $4, \
        cpu, mem, io, maxvmem
}

function getUsageLine(line) {
    split(line, qdelOutput, "!!!")
    for (i in qdelOutput) {
        if (qdelOutput[i] ~ /^usage/) {
            return qdelOutput[i]
        }
    }
}

function convertTimeToSecs(value) {
    split(value, time, ":")
    return time[1] * 3600 + time[2] * 60 + time[3]
}

function convertToMBs(value) {
    unit = substr(value, length(value), 1)
    if (unit == "K") {
        value /= 1024
    } else if (unit == "M") {
        value *= 1024 * 1024
    } else if (unit == "G") {
        value *= 1024 * 1024 * 1024
    } else if (unit == "T") {
        value *= 1024 * 1024 * 1024 * 1024
    }
    return value / 1024 / 1024
}
