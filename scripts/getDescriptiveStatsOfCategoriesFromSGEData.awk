# Copyright (c) 2018, Kansas State University
#
# BSD 3-clause License
#
# Author: Venkatesh-Prasad Ranganath

BEGIN {
    while ((getline < feedbackFile) > 0) {
        split($0, tmp1, ",")
        gsub(/_/, "", tmp1[1])
        job_cat[tmp1[1]] = tmp1[5]
        category_wc_time[tmp1[5]] = 0
        category_cpu_time[tmp1[5]] = 0
        category_freq[tmp1[5]] += 1
        total_freq += 1
        if (tmp1[6] > 0) {
          category_exec[tmp1[5]] += 1
          total_exec += 1
        }
        job_execed[tmp1[1]] = 0
    }
    FS = ":"
}

{
    if ($6 in job_cat && $10 < $11 && $37 > 0) {
        category_wc_time[job_cat[$6]] += ($11 - $10)
        category_cpu_time[job_cat[$6]] += $37
        job_execed[job_cat[$6], $6] = 1
    }
}

END {
    for (i in category_freq) {
        for (j in job_execed) {
            split(j, tmp1, SUBSEP)
            if (tmp1[1] == i) {
                category_execed[i] += 1
                total_execed += 1
            }
        }
    }

    for (i in category_wc_time) {
        total_wc_time += category_wc_time[i]
        total_cpu_time += category_cpu_time[i]
    }

    print "Reason,Frequency,Executed,Executed %," \
        "CPU Time (s),CPU Time %,WC Time (s),WC Time %"
    for (i in category_wc_time) {
        printf "%s,%d,%d,%03.2f,%d,%03.2f,%d,%03.2f\n", i, category_freq[i], \
            category_execed[i], 100 * category_execed[i] / total_execed, \
            category_cpu_time[i], 100 * category_cpu_time[i] / total_cpu_time, \
            category_wc_time[i], 100 * category_wc_time[i] / total_wc_time
    }
    printf "Total,%d,%d,%d,%d,%d,%d,%d\n", total_freq, total_execed, 100, \
        total_cpu_time, 100, total_wc_time, 100
}
