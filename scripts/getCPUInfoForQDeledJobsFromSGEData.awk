# Copyright (c) 2018, Kansas State University
#
# BSD 3-clause License
#
# Author: Venkatesh-Prasad Ranganath

BEGIN {
    while ((getline < feedbackFile) > 0) {
        split($0, tmp1, ",")
        gsub(/_/, "", tmp1[1])
        qdel_time[tmp1[1]] = tmp1[6]
    }
    FS = ":"
}

{
    if ($6 in qdel_time && $10 < $11) {
        sge_wall_clock_run_time[$6, $13] += ($11 - $10)
        sge_wall_clock_cpu_run_time[$6, $13] += ($11 - $10) * $35
        sge_cpu_run_time[$6, $13] += $37
        sge_alloc_cores[$6, $13] += $35
    }
}

END {
    print "Job Id,Qdel Time (s),SGE Wall Clock Run Time (s)," \
        "SGE Wall Clock CPU Run Time (s),SGE CPU Run Time (s)," \
        "# Alloc Cores,Exit Status,Untransformed Exit Status"
    for (i in sge_cpu_run_time) {
        split(i, tmp1, SUBSEP)
        jobId = tmp1[1]
        status = tmp1[2]
        printf "%d,%d,%d,%d,%d,%d,%d,%d\n", jobId, qdel_time[jobId], \
            sge_wall_clock_run_time[i], sge_wall_clock_cpu_run_time[i], \
            sge_cpu_run_time[i], sge_alloc_cores[i], status, status - 128
    }
}
