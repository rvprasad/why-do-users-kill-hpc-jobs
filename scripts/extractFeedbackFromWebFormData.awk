# Copyright (c) 2017, Kansas State University
#
# BSD 3-clause License
#
# Author: Venkatesh-Prasad Ranganath

BEGIN {
    while ((getline < rawFeedbackFile) > 0) {
        gsub(/"/, "", $0)
        if ($0 ~ /^[0-9].*/) {
            split($0, tmp1, ",")
            split(tmp1[3], tmp2, "_")
            for (i in tmp2) {
                jobId = tmp2[i]
                qdel_info[jobId] = tmp1[2] "," jobId "," tmp1[5] "," tmp1[4]
            }
        }
    }
    FS = ":"
}

NR > 1 {
    if ($6 in qdel_info) {
        cpu_info[$6] += $37
        mem = convertToMBs($38 "G")
        if (mem_info[$6] < mem) {
            mem_info[$6] = mem
        }
        io_info[$6] += convertToMBs($39 "G")
        max_vmem = convertToMBs($43)
        if (max_vmem_info[$6] < max_vmem) {
            max_vmem_info[$6] = max_vmem
        }
    }
}

END {
    for (jobId in qdel_info) {
        # username, jobid, app, reason, cpu (s), mem (MBs), io (MB), maxvmem (MB)
        printf "%s,%.3f,%.3f,%.3f,%.3f\n", qdel_info[jobId], \
            cpu_info[jobId], mem_info[jobId], io_info[jobId], \
            max_vmem_info[jobId]
    }
}

function convertToMBs(value) {
    unit = substr(value, length(value), 1)
    if (unit == "K") {
        value /= 1024
    } else if (unit == "M") {
        value *= 1024 * 1024
    } else if (unit == "G") {
        value *= 1024 * 1024 * 1024
    } else if (unit == "T") {
        value *= 1024 * 1024 * 1024 * 1024
    }
    return value / 1024 / 1024
}
