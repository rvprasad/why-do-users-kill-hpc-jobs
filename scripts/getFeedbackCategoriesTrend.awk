# Copyright (c) 2017, Kansas State University
#
# BSD 3-clause License
#
# Author: Venkatesh-Prasad Ranganath

BEGIN {
    FS = ","
    line = ""
    num_of_categories = 0
    while ((getline < categoriesFile) > 0) {
        categories_2_freq[$1] = 0
        line = line "," $1
        num_of_categories += 1
        idx_2_category[num_of_categories] = $1
    }
    print line ", unique users"
}

{
    users[$2] += 1
    categories_2_freq[$5] += 1

    if (NR % 1000 == 0) {
        printTrendPoint(NR, users, categories_2_freq)
    }
}

END {
    if (NR % 1000 != 0) {
        printTrendPoint(NR, users, categories_2_freq)
    }
}

function printTrendPoint(num_of_records, users, categories) {
    line = num_of_records
    num_of_unique_users = 0
    for (i in users) {
        if (users[i] != 0) {
            num_of_unique_users += 1
        }
        users[i] = 0
    }
    for (i = 1; i <= num_of_categories; i++s) {
        j = idx_2_category[i]
        line = line "," categories[j]
        categories[j] = 0
    }
    print line "," num_of_unique_users
}
