# Copyright (c) 2017, Kansas State University
#
# BSD 3-clause License
#
# Author: Venkatesh-Prasad Ranganath 

BEGIN {
    FS = ","
    category = 0
    OFS = ""
    print "s/,,,/,,,u,/g" # Unkonwn category
}

{
    feedback = "," $2 "," $3 "," $4 ","
    gsub(/\//, "\\\/", feedback)
    gsub(/\[/, "\\[", feedback)
    gsub(/\+/, "\\+", feedback)
    gsub(/\?/, "\\?", feedback)
    gsub(/\(/, "\\(", feedback)
    gsub(/\)/, "\\)", feedback)
    print "s/", feedback, "/&", $1, ",/g"
}
