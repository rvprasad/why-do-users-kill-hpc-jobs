# Copyright (c) 2018, Kansas State University
#
# BSD 3-clause License
#
# Author: Venkatesh-Prasad Ranganath

BEGIN {
    FS = ":"
}

{
    if ($9 < $11) {
        wall_clock_wait_time = $10 - $9
        wall_clock_run_time = ($11 - $10)
        wall_clock_cpu_run_time = ($11 - $10) * $35
        cpu_run_time = $37

        if ($13 > 127) {
            abnormal_wall_clock_wait_time[$13] += wall_clock_wait_time
            abnormal_wall_clock_run_time[$13] += wall_clock_run_time
            abnormal_wall_clock_cpu_run_time[$13] += wall_clock_cpu_run_time
            abnormal_cpu_run_time[$13] += cpu_run_time
        } else {
            normal_wall_clock_wait_time[$13] += wall_clock_wait_time
            normal_wall_clock_run_time[$13] += wall_clock_run_time
            normal_wall_clock_cpu_run_time[$13] += wall_clock_cpu_run_time
            normal_cpu_run_time[$13] += cpu_run_time
        }
    }
}

END {
    print "Normal Exit,Exit Status,Untransformed Exit Status," \
        "Wall Clock Wait Time (s),Wall Clock Run Time (s)," \
        "Wall Clock Job Time (s),Wall Clock CPU Run Time (s),CPU Run Time (s)"

    writeData("Y", normal_wall_clock_wait_time, normal_wall_clock_run_time,
        normal_wall_clock_cpu_run_time, normal_cpu_run_time)
    writeData("N", abnormal_wall_clock_wait_time, abnormal_wall_clock_run_time,
        abnormal_wall_clock_cpu_run_time, abnormal_cpu_run_time)
}

function writeData(normal_exit, arr1, arr2, arr3, arr4) {
    for (signal in arr1) {
        wall_clock_wait_time = arr1[signal]
        wall_clock_run_time = arr2[signal]
        wall_clock_cpu_run_time = arr3[signal]
        cpu_run_time = arr4[signal]
        wall_clock_job_time = wall_clock_wait_time + wall_clock_run_time
        if (normal_exit == "N") {
            untransformed_signal = signal - 128
        } else {
            untransformed_signal = signal
        }
        printf "%s,%d,%d,%d,%d,%d,%d,%d\n", normal_exit, signal,\
            untransformed_signal, wall_clock_wait_time, wall_clock_run_time,\
            wall_clock_job_time, wall_clock_cpu_run_time, cpu_run_time
    }
}
