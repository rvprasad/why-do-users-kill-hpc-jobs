# Copyright (c) 2017, Kansas State University
#
# BSD 3-clause License
#
# Author: Venkatesh-Prasad Ranganath

BEGIN {
    FS = ","
}

{
    freq[$5] += 1
    cpu[$5] += $6
    mem[$5] += $7
    io[$5] += $8
    maxvmem[$5] += $9
    if ($6 > 0) {
        executed[$5] += 1
    }
}

END {
    total = 0
    for (i in freq) {
        total_freq += freq[i]
        total_cpu += cpu[i]
        total_mem += mem[i]
        total_io += io[i]
        total_maxvmem += maxvmem[i]
        total_executed += executed[i]
    }
    print "Category,Frequency,# of Executed,Executed %,cpu (s),cpu %," \
        "mem (GBs),io (GB),maxvmem (GB)"
    for (i in freq) {
        printf "%s,%d,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n", i, freq[i],\
          executed[i], 100 * executed[i] / total_executed, cpu[i], \
          100 * cpu[i] / total_cpu, mem[i], io[i], maxvmem[i]
    }
    printf "Total,%d,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n", total_freq, \
        total_executed, 100, total_cpu, 100, total_mem, total_io, \
        total_maxvmem, total_executed
}
