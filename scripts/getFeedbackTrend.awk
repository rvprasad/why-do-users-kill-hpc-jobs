# Copyright (c) 2017, Kansas State University
#
# BSD 3-clause License
#
# Author: Venkatesh-Prasad Ranganath

BEGIN {
    FS = ","
    while ((getline < qdelFeedbackFile) > 0) {
        if (!($1 ~ /"[[:alpha:]]+"/)) {
            split($1, tmp1, " ")
            key = tmp1[1]
            gsub(/\//, "-", key)
            gsub(/"/, "", key)
            date_2_non_empty_feedback[key] += 1
            date_2_feedback[key] += 1
            keys[key] = key
        }
    }
}

{
    key = FILENAME
    gsub(/.csv/, "", key)
    gsub(/.*output-/, "", key)
    if ($4 != "") {
        date_2_non_empty_feedback[key] += 1
    }
    date_2_feedback[key] += 1
    keys[key] = key
}

END {
    print "Day,# of Feedbacks,# of Non-Empty Feedback,"
    for (i in keys) {
        printf "%s,%d,%d\n", i, date_2_feedback[i], date_2_non_empty_feedback[i]
    }
}
