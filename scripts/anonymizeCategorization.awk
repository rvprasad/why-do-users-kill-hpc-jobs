# Copyright (c) 2017, Kansas State University
#
# BSD 3-clause License
#
# Author: Venkatesh-Prasad Ranganath 

BEGIN {
    FS = ","
    numOfUsers = 0
    OFS = ","

    while ((getline < user2idFile) > 0) {
        users[$1] = $2
    }
}

{
    print $1, users[$2], $3, $4
}
