# Copyright (c) 2017, Kansas State University
#
# BSD 3-clause License
#
# Author: Venkatesh-Prasad Ranganath

BEGIN {
    while ((getline < webFeedbackFile) > 0) {
        split($0, tmp1, ",")
        userIdJobId_2_feedback[tmp1[1], tmp1[2]] = $0
        unconsolidated[tmp1[1], tmp1[2]] = $0
    }
    FS = ","
}

{
    if ($2 ~ /^[[:digit:]]+$/) {
        if (($1, $2) in userIdJobId_2_feedback) {
            print userIdJobId_2_feedback[$1, $2]
            delete unconsolidated[$1, $2]
        } else {
            print $0
        }
    }
}

END {
    for (i in unconsolidated) {
        print unconsolidated[i]
    }
}
